#!/bin/bash
#[ -f /etc/init.d/functions ] && . /etc/init.d/functions
err_log=./err.log

if [[ ! -f ${err_log} ]];then
  touch ./err.log
fi
##
if [[ ! -f hosts ]];then
cat >>hosts<<EOF
127.0.0.1
10.25.171.24
10.168.235.26
10.47.108.240
EOF
fi

task_list=(
cqado-web
cqaso-web
cqaso-admin
cqaso-mobile
)

version=(
dev
master
)
##
clear
echo "开始检查`hostname` 机器是否安装ansible环境";
if [[ `uname` == 'Darwin' ]];then
  brew list | grep ansible >>/dev/null
  if [[ $? -ne 0 ]];then
    echo "该机器没有安装ansible环境，即将开始安装ansible"
    which pip3
    if [ $? -ne 0 ];then
        brew install python3
        pip3 install ansible
    fi
  else
    echo "`hostname` 已安装ansible"
  fi
else
  echo "ansible 已安装成功"
fi
##
if [[ `uname` == 'Linux' ]];then
    rpm -qa | grep ansible >>/dev/null
    if [[ $? -ne 0 ]];then
        echo "该机器没有安装ansible环境，即将开始安装ansible"
        echo "`hostname` 主机正在安装ansible,请稍等"
        yum install -y ansible
        pip3 install -r requirements.txt
    else
        echo "`hostname`已安装ansible"
    fi
fi

#
function menu(){
cat << EOF
----------------------------------------------
|*******Please Enter Your Choice:[1-5]*******|
----------------------------------------------
   `echo -e "\033[35m 1) ${task_list[0]} deploy \033[0m"`
   `echo -e "\033[35m 2) ${task_list[1]} deploy \033[0m"`
   `echo -e "\033[35m 3) ${task_list[2]} deploy \033[0m"`
   `echo -e "\033[35m 4) ${task_list[3]} deploy \033[0m"`
   `echo -e "\033[35m 5) quit \033[0m"`
EOF
}

function Cqado(){
cat << EOF
--------------------------------------------------
|*******Please Enter Your Choice:[1-3]***********|
--------------------------------------------------
   `echo -e "\033[36m 1) ${task_list[0]}-${version[0]} \033[0m"`
   `echo -e "\033[36m 3) ${task_list[0]}-${version[1]} \033[0m"`
   `echo -e "\033[36m 2) ${task_list[0]}-${version[0]}-rollback \033[0m"`
   `echo -e "\033[36m 4) ${task_list[0]}-${version[1]}-rollback \033[0m"`
   `echo -e "\033[36m 5) return main menu \033[0m"`
EOF
read -p "please input your choice: " num2
expr $num2 + 1 &>/${version[0]}/null
if [[ $? -ne 0 ]];then
    echo "###########################"
    echo "Waring!!!,input error "
    echo "Please enter choose[1-3]:"
    echo "###########################"
fi
#
if [[ $num2 -eq 1 ]];then
    echo "${task_list[0]}-${version[0]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
    echo "正在部署${task_list[0]}-${version[0]}，请稍等"
    time ansible-playbook -i hosts ${task_list[0]}/${task_list[0]}-${version[0]}-build.yml -v 2>>err.log >>err.log
    if [[ $? -eq 0 ]];then
        time ansible-playbook -i hosts ${task_list[0]}/${task_list[0]}-${version[0]}-deploy.yml -v 2>>err.log >>err.log
        if [[ $? -eq 0 ]];then
            echo "任务${task_list[0]}-${version[0]}，已部署完成"
        else
            echo "任务${task_list[0]}-${version[0]}部署失败，请查看日志err.log"
        fi
    else
        echo "${task_list[0]}/${task_list[0]}-${version[0]}-build.yml 部署失败，请查看日志"
    fi

elif [[ $num2 -eq 2 ]]; then
    echo "${task_list[0]}-${version[1]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')"  2>>err.log >>err.log
    echo "正在部署${task_list[0]}-${version[1]}，请稍等"
    time ansible-playbook -i hosts ${task_list[0]}/${task_list[0]}-${version[1]}-build.yml -v 2>>err.log >>err.log
    if [[ $? -eq 0 ]];then
       time ansible-playbook -i hosts ${task_list[0]}/${task_list[0]}-${version[1]}-deploy.yml -v 2>>err.log >>err.log
       if [[ $? -eq 0 ]];then
           echo "任务${task_list[0]}-${version[1]}，已部署完成"
       else
           echo "${task_list[0]}/${task_list[0]}-${version[1]}-deploy.yml执行失败，请查看日志"
       fi
    else
        echo "${task_list[0]}/${task_list[0]}-${version[1]}-build.yml执行失败，请查看日志"
    fi

elif [[ $num5 -eq 3 ]]; then
  echo "${task_list[0]}-${version[0]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
  time ansible-playbook -i hosts ${task_list[0]}/${task_list[0]}-${version[0]}-rollback.yml -v 2>>err.log >>err.log
  if [[ $? -eq 0 ]];then
      echo "任务${task_list[0]}-${version[0]}，已部署完成"
  else
      echo "${task_list[0]}/${task_list[0]}-${version[0]}-rollback.yml部署失败，请查看err.log"
  fi

elif [[ $num5 -eq 4 ]]; then
  echo "${task_list[0]}-${version[1]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
  time ansible-playbook -i hosts ${task_list[0]}/${task_list[0]}-${version[1]}-rollback.yml -v 2>>err.log >>err.log
  if [[ $? -eq 0 ]];then
      echo "任务${task_list[0]}-${version[1]}，已部署完成"
  else
      echo "${task_list[0]}/${task_list[0]}-${version[1]}-rollback.yml部署失败，请查看err.log"
  fi

elif [[ $num2 -eq 5 ]]; then
  clear
fi
}

function CqasoWeb(){
cat << EOF
--------------------------------------------------
|*******Please Enter Your Choice:[1-3]***********|
--------------------------------------------------
   `echo -e "\033[36m 1) ${task_list[1]}-${version[0]} \033[0m"`
   `echo -e "\033[36m 3) ${task_list[1]}-${version[1]} \033[0m"`
   `echo -e "\033[36m 2) ${task_list[1]}-${version[0]}-rollback \033[0m"`
   `echo -e "\033[36m 4) ${task_list[1]}-${version[1]}-rollback \033[0m"`
   `echo -e "\033[36m 5) return main menu \033[0m"`
EOF
read -p "please input your choice: " num3
expr $num3 + 1 &>/${version[0]}/null
if [[ $? -ne 0 ]];then
  echo "###########################"
  echo "Waring!!!,input error "
  echo "Please enter choose[1-3]:"
  echo "###########################"
fi
##
if [[ $num3 -eq 1 ]];then
    echo "${task_list[1]}-${version[0]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
    echo "正在部署${task_list[1]}-${version[0]}，请稍等"
    time ansible-playbook -i hosts ${task_list[1]}/${task_list[1]}-${version[0]}-build.yml -v 2>>err.log >>err.log
  if [[ $? -eq 0 ]];then
      time ansible-playbook -i hosts ${task_list[1]}/${task_list[1]}-${version[0]}-deploy.yml -v 2>>err.log >>err.log
      if [[ $? -eq 0 ]];then
          echo "任务${task_list[1]}-${version[0]}，已部署完成"
      else
          echo "${task_list[1]}/${task_list[1]}-${version[0]}-deploy.yml部署失败，请查看err.log"
      fi
  else
      echo "${task_list[1]}/${task_list[1]}-${version[0]}-build.yml部署失败，请查看err.log"
  fi

elif [[ $num3 -eq 2 ]]; then
  echo "${task_list[1]}-${version[1]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
  echo "正在部署${task_list[1]}-${version[1]}，请稍等"
  time ansible-playbook -i hosts ${task_list[1]}/${task_list[1]}-${version[1]}-build.yml -v 2>>err.log >>err.log
  if [[ $? -eq 0 ]];then
      time ansible-playbook -i hosts ${task_list[1]}/${task_list[1]}-${version[1]}-deploy.yml -v 2>>err.log >>err.log
      if [[ $? -eq 0 ]];then
          echo "任务${task_list[1]}-${version[1]}，已部署完成"
      else
          echo "${task_list[1]}/${task_list[1]}-${version[1]}-deploy.yml部署失败，请查看err.log"
      fi
  else
      echo "${task_list[1]}/${task_list[1]}-${version[1]}-build.yml部署失败，请查看err.log"
  fi

elif [[ $num5 -eq 3 ]]; then
  echo "${task_list[3]}-${version[1]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
  time ansible-playbook -i hosts ${task_list[1]}/${task_list[1]}-${version[0]}-rollback.yml -v 2>>err.log >>err.log
  if [[ $? -eq 0 ]];then
      echo "任务${task_list[1]}-${version[0]}，已部署完成"
  else
      echo "${task_list[3]}/${task_list[1]}-${version[0]}-rollback.yml部署失败，请查看err.log"
  fi

elif [[ $num5 -eq 4 ]]; then
  echo "${task_list[3]}-${version[1]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
  time ansible-playbook -i hosts ${task_list[1]}/${task_list[1]}-${version[1]}-rollback.yml -v 2>>err.log >>err.log
  if [[ $? -eq 0 ]];then
      echo "任务${task_list[1]}-${version[1]}，已部署完成"
  else
      echo "${task_list[1]}/${task_list[1]}-${version[1]}-rollback.yml部署失败，请查看err.log"
  fi

elif [[ $num3 -eq 5 ]]; then
  clear
fi
}

function CqasoAdmin(){
cat << EOF
--------------------------------------------------
|*******Please Enter Your Choice:[1-2]***********|
--------------------------------------------------
   `echo -e "\033[36m 1) ${task_list[2]}-${version[0]} \033[0m"`
#   `echo -e "\033[36m 2) ${task_list[2]}-${version[1]} \033[0m"`
   `echo -e "\033[36m 2) return main menu \033[0m"`
EOF
read -p "please input your choice: " num4
expr $num4 + 1 &>/${version[0]}/null
if [[ $? -ne 0 ]];then
  echo "###########################"
  echo "Waring!!!,input error "
  echo "Please enter choose[1-2]:"
  echo "###########################"
fi
#
if [[ $num4 -eq 1 ]];then
  echo "${task_list[2]}-${version[0]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
  time ansible-playbook -i hosts ${task_list[2]}/${task_list[2]}-${version[0]}-build.yml -v 2>>err.log >>err.log
  if [[ $? -eq 0 ]];then
      time ansible-playbook -i hosts ${task_list[2]}/${task_list[2]}-${version[0]}-deploy.yml -v 2>>err.log >>err.log
      if [[ $? -eq 0 ]];then
          echo "任务${task_list[2]}-${version[0]}，已部署完成"
      else
          echo "${task_list[2]}/${task_list[2]}-${version[0]}-deploy.yml部署失败，请查看err.log"
      fi
  else
      echo "${task_list[2]}/${task_list[2]}-${version[0]}-build.yml部署失败，请查看err.log"
  fi

#elif [[ $num4 -eq 2 ]]; then
#  echo "${task_list[2]}-${version[1]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
#  ansible-playbook -i hosts ${task_list[2]}/${task_list[2]}-${version[1]}-build.yml -v 2>>err.log >>err.log
#  if [[ $? -eq 0 ]];then
#      ansible-playbook -i hosts ${task_list[2]}/${task_list[2]}-${version[1]}-deploy.yml -v 2>>err.log >>err.log
#      if [[ $? -eq 0 ]];then
#          echo "任务${task_list[2]}-${version[1]}，已部署完成"
#      else
#          echo "${task_list[2]}/${task_list[2]}-${version[1]}-deploy.yml部署失败，请查看err.log"
#      fi
#  else
#      echo "${task_list[2]}/${task_list[2]}-${version[1]}-build.yml部署失败，请查看err.log"
#  fi
else
  clear
fi
}

function CqasoMoble(){
cat << EOF
--------------------------------------------------
|*******Please Enter Your Choice:[1-3]***********|
--------------------------------------------------
   `echo -e "\033[36m 1) ${task_list[3]}-${version[0]} \033[0m"`
   `echo -e "\033[36m 3) ${task_list[3]}-${version[0]} \033[0m"`
   `echo -e "\033[36m 2) ${task_list[3]}-${version[0]}-rollback \033[0m"`
   `echo -e "\033[36m 4) ${task_list[3]}-${version[1]}-rollback \033[0m"`
   `echo -e "\033[36m 5) return main menu \033[0m"`
EOF
read -p "please input your choice: " num5
expr $num5 + 1 &>/${version[0]}/null
if [[ $? -ne 0 ]];then
  echo "###########################"
  echo "Waring!!!,input error "
  echo "Please enter choose[1-3]:"
  echo "###########################"
fi
#
if [[ $num5 -eq 1 ]];then
  echo "${task_list[3]}-${version[0]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')"  2>>err.log >>err.log
  echo "正在部署${task_list[3]}-${version[0]}，请稍等"
  time ansible-playbook -i hosts ${task_list[3]}/${task_list[3]}-${version[0]}-build.yml -v 2>>err.log >>err.log
  if [[ $? -eq 0 ]];then
      time ansible-playbook -i hosts ${task_list[3]}/${task_list[3]}-${version[0]}-deploy.yml -v 2>>err.log >>err.log
      if [[ $? -eq 0 ]];then
          echo "任务${task_list[3]}-${version[0]}，已部署完成"
      else
          echo "${task_list[3]}/${task_list[3]}-${version[0]}-deploy.yml部署失败，请查看err.log"
      fi
  else
      echo "${task_list[3]}/${task_list[3]}-${version[0]}-build.yml部署失败，请查看err.log"
  fi

elif [[ $num5 -eq 2 ]]; then
  echo "${task_list[3]}-${version[1]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
  time ansible-playbook -i hosts ${task_list[3]}/${task_list[3]}-${version[1]}-build.yml -v 2>>err.log >>err.log
  if [[ $? -eq 0 ]];then
      time ansible-playbook -i hosts ${task_list[3]}/${task_list[3]}-${version[1]}-deploy.yml -v 2>>err.log >>err.log
      if [[ $? -eq 0 ]];then
          echo "任务${task_list[3]}-${version[1]}，已部署完成"
      else
          echo "${task_list[3]}/${task_list[3]}-${version[1]}-deploy.yml部署失败，请查看err.log"
      fi
  else
      echo "${task_list[3]}/${task_list[3]}-${version[1]}-build.yml部署失败，请查看err.log"
  fi

elif [[ $num5 -eq 3 ]]; then
  echo "${task_list[3]}-${version[1]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
  time ansible-playbook -i hosts ${task_list[3]}/${task_list[3]}-${version[0]}-rollback.yml -v 2>>err.log >>err.log
  if [[ $? -eq 0 ]];then
      echo "任务${task_list[3]}-${version[0]}，已部署完成"
  else
      echo "${task_list[3]}/${task_list[3]}-${version[0]}-rollback.yml部署失败，请查看err.log"
  fi

elif [[ $num5 -eq 4 ]]; then
  echo "${task_list[3]}-${version[1]} 开始执行时间：$(date +'%Y%m%d %H:%M:%S')" 2>>err.log >>err.log
  time ansible-playbook -i hosts ${task_list[3]}/${task_list[3]}-${version[1]}-rollback.yml -v 2>>err.log >>err.log
  if [[ $? -eq 0 ]];then
      echo "任务${task_list[3]}-${version[1]}，已部署完成"
  else
      echo "${task_list[3]}/${task_list[3]}-${version[1]}-rollback.yml部署失败，请查看err.log"
  fi


elif [[ $num5 -eq 5 ]]; then
  clear
fi
}

while true ;do
  menu
  read -p "##please Enter Your first_menu Choice:[1-5]: " num1
  expr $num1 + 1 &>/${version[0]}/null   #这里加1，判断输入的是不是整数。
  if [ $? -ne 0 ];then   #如果不等于零，代表输入不是整数。
    echo "----------------------------"
    echo "|      Waring!!!           |"
    echo "|Please Enter Right Choice!|"
    echo "----------------------------"
  fi
  case $num1 in
      1)
       clear
       Cqado
       ;;
      2)
       clear
       CqasoWeb
       ;;
      3)
       clear
       CqasoAdmin
       ;;
      4)
       clear
       CqasoMoble
       ;;
      5)
       exit 2
       ;;
     *)
      clear
      echo -e "\033[31m Your Enter a number Error,Please Enter again Choice [1-5]: \033[0m"
  esac
done
